const express = require("express");
const mysql = require("mysql2");
const cors = require("cors");
const bodyparser = require("body-parser");


const app = express();
app.use(cors('*'));
app.use(bodyparser.json());

var mysqlConnection = mysql.createConnection({
    host: 'host.docker.internal',
    port: 6900,
    user: 'root',
    password: 'manager',
    database: 'akshay'
});

mysqlConnection.connect((err)=> {
if(!err)
console.log('Connection Established Successfully');
else
console.log('Connection Failed!');
});


app.get("/", (req,res)=>{
    res.send("Hello everyone");
})

// reading the data
app.get("/read",(req,res)=>{
    mysqlConnection.query("SELECT * from Movie_Tb",(err, result)=>{
        if(!err){
            res.send(result);
        }else{
            console.log(err);
        }
    });
});

// reading data of specific id
app.get("/read/:movie_id", (req,res)=>{
    mysqlConnection.query("select * from Movie_Tb where movie_id=?", [req.params.movie_id], (err, result)=>{
        if(!err){
            res.send(result);
        }else{
            console.log(err);
        }
    });
})

// inserting data
app.post("/read",(req,res)=>{
    var movieval = req.body;
    mysqlConnection.query("INSERT into Movie_Tb values(?, ?, ?, ?, ?)",[movieval.movie_id, movieval.movie_title, movieval.movie_release_date, movieval.movie_time, movieval.director_name],(err,result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    } );
});

//deleting data by id
// inserting data
app.delete("/read/:movie_id",(req,res)=>{
    var movieval = req.body;
    mysqlConnection.query("delete from Movie_Tb where movie_id=?",[req.params.movie_id],(err,result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    } );
});

//updating data
app.put("/read",(req,res)=>{
    var movieval = req.body;
    mysqlConnection.query("UPDATE Movie_Tb SET movie_title=?, movie_release_date=?, movie_time=?, director_name=? where movie_id=?",[movieval.movie_title, movieval.movie_release_date, movieval.movie_time, movieval.director_name, movieval.movie_id],(err,result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    });
});


const port = 3000;
app.listen(port, ()=>{
    console.log('Express listening');
})